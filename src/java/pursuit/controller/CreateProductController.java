/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package pursuit.controller;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import pursuit.dao.ProductDAO;

/**
 *
 * @author Windows
 */
public class CreateProductController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
        String productName = request.getParameter("productName").trim();
        int category = Integer.parseInt(request.getParameter("category"));
        int unitsInStock = Integer.parseInt(request.getParameter("unitsInStock"));
        int unitsOnOrder = Integer.parseInt(request.getParameter("unitsOnOrder"));
        boolean discontinued = Boolean.parseBoolean(request.getParameter("discontinued"));
        String description = request.getParameter("description").trim();
        
        try {
            ProductDAO pdao = new ProductDAO();
            boolean check = pdao.addProduct(category, productName, description, unitsInStock, unitsOnOrder, discontinued);
            
            if (check) {
                request.setAttribute("CREATE_SUCCESS", "Create Successful!");
                request.getRequestDispatcher("ProductController").include(request, response);
            } else {
                request.setAttribute("CREATE_ERROR", "Create Failed!");
            }
        } catch (Exception e) {
            log("Error at CreateProductController: " + e.toString());
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
