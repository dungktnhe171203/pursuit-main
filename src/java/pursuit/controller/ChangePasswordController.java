/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package pursuit.controller;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import pursuit.dao.AccountDAO;
import pursuit.dto.AccountDTO;
import pursuit.utils.Email;
import pursuit.utils.Encode;

/**
 *
 * @author Windows
 */
public class ChangePasswordController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
       String email= request.getParameter("email");
       String oldPassword = request.getParameter("oldPassword");
        String newPassword = request.getParameter("password");
        Gson gson = new Gson();
        Map<String, Object> data = new HashMap<>();

        try {
            HttpSession session = request.getSession();
            AccountDTO accountDTO = (AccountDTO) session.getAttribute("USER");
            AccountDAO accountDAO = new AccountDAO();
            boolean check = accountDAO.checkPassword(accountDTO.getAccountId(), accountDTO.getCustomer().getCustomerId(), Encode.toSHA1(oldPassword));
            String rootPath = request.getScheme() + "://" + request.getServerName()
                    + ":" + request.getServerPort() + request.getContextPath();
            boolean isSendEmail = Email.sendEmail(email, "Welcome to Pursuit!", Email.EmailFogotPassWord(rootPath, oldPassword, newPassword));
            if (check && isSendEmail) {
                check = accountDAO.setPassword(accountDTO.getAccountId(), accountDTO.getCustomer().getCustomerId(), Encode.toSHA1(newPassword));
                
                if (check && isSendEmail) {
                    data.put("SUCCESS", "Your password has been changed successfully");
                }
            } else {
                data.put("ERROR", "Incorrect old password. Please re-enter your old password to proceed with the password change process");
            }
        } catch (Exception e) {
            log("Error at LoginController: " + e.toString());
        } finally {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(gson.toJson(data));
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
