<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Pursuit</title>

        <!-- Check URL -->
        <script src="./assets/js/shop.js"></script>

        <!-- Embed Favicon -->
        <link
            rel="apple-touch-icon"
            sizes="57x57"
            href="./assets/favicon/apple-icon-57x57.png"
        />
        <link
            rel="apple-touch-icon"
            sizes="60x60"
            href="./assets/favicon/apple-icon-60x60.png"
        />
        <link
            rel="apple-touch-icon"
            sizes="72x72"
            href="./assets/favicon/apple-icon-72x72.png"
        />
        <link
            rel="apple-touch-icon"
            sizes="76x76"
            href="./assets/favicon/apple-icon-76x76.png"
        />
        <link
            rel="apple-touch-icon"
            sizes="114x114"
            href="./assets/favicon/apple-icon-114x114.png"
        />
        <link
            rel="apple-touch-icon"
            sizes="120x120"
            href="./assets/favicon/apple-icon-120x120.png"
        />
        <link
            rel="apple-touch-icon"
            sizes="144x144"
            href="./assets/favicon/apple-icon-144x144.png"
        />
        <link
            rel="apple-touch-icon"
            sizes="152x152"
            href="./assets/favicon/apple-icon-152x152.png"
        />
        <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="./assets/favicon/apple-icon-180x180.png"
        />
        <link
            rel="icon"
            type="image/png"
            sizes="192x192"
            href="./assets/favicon/android-icon-192x192.png"
        />
        <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="./assets/favicon/favicon-32x32.png"
        />
        <link
            rel="icon"
            type="image/png"
            sizes="96x96"
            href="./assets/favicon/favicon-96x96.png"
        />
        <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="./assets/favicon/favicon-16x16.png"
        />
        <link rel="manifest" href="./assets/favicon/manifest.json" />
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta
            name="msapplication-TileImage"
            content="./assets/favicon/ms-icon-144x144.png"
        />
        <meta name="theme-color" content="#ffffff" />

        <!-- Embed Font Awesome -->
        <link
            rel="preconnect"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        />
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
            integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"
        />

        <!-- Styles CSS -->
        <link rel="stylesheet" href="./assets/css/main.css" />
         <link rel="stylesheet" href="./assets/css/index.css" />
    </head>
    <body>
        <c:import url="./header.jsp" />
         <section class="hero">
      <div class="container">
                <div class="hero__inner">
                    <!-- Hero content -->
                    <section class="hero__content">
                        <h1 class="hero__heading">
                            Summer - Fall Collections 2023
                        </h1>
                        <p class="hero__desc">
                            The most wanted styles is waiting for you. Find the best styles of modern shoes for you.
                            Still, the second option holds promised. could make the tagline.
                        </p>
                        <button type="submit" class="hero-action">
                            <a  href="http://localhost:9999/pursuit/MainController?action=Product" class=" hero-action__booking">
                                Explore Product
                            </a>
                        
                        </button>
                    </section>
                    <!-- Hero media -->
                    <figure class="hero__media">
                        <img
                            src="./assets/imgs/first.jpg"
                            alt="Online catalog management tool"
                            class="hero__media-img"
                            />
                    </figure>
                </div>
                </section>
                <!--banner-->
                <main>
              <section class="grid wide">
                  <div class="container">
                    <h1 class="section-heading">
                        NEW ARRIVALS
                    </h1>
                <div class="row">
                    <div class="col l-7 l-o-4 m-12 m-o-0 c-10 c-o-1">
                        <div class="banner__item">
                            <div class="banner__item-img">
                                <img src="./assets/imgs/banner-1.jpg" alt="">
                            </div>
                            <div class="banner__item-text">
                                <h2>Clothing Collections </h2>
                                <a href="http://localhost:9999/pursuit/MainController?action=Product">Shop now</a>
                            </div>
                        </div>
                    </div>
            
                    <div class="col l-4 m-12 m-o-0 c-10 c-o-1">
                        <div class="banner__item banner__item--middle">
                            <div class="banner__item-img">
                                <img src="./assets/imgs/banner-2.jpg" alt="">
                            </div>
                            <div class="banner__item-text">
                                <h2>Accessories</h2>
                                <a href="http://localhost:9999/pursuit/MainController?action=Producthttp://localhost:9999/pursuit/MainController?action=Product">
                                    Shop now</a>
                            </div>
                        </div>
                    </div>
            
                    <div class="col l-8 m-12 m-o-0 c-10 c-o-1">
                        <div class="banner__item banner__item--last">
                            <div class="banner__item-img">
                                <img src="./assets/imgs/banner-3.jpg" alt="">
                            </div>
                            <div class="banner__item-text ">
                                <h2>Shoes Spring </h2>
                                <a href="http://localhost:9999/pursuit/MainController?action=Product&categoryID=5">
                                    Shop now</a>
                            </div>
                        </div>
                    </div>            
                </div>
        </div>
                </section>
                    <section class="sale">
                        <div class ="container saler">
                        <div class=" c-6 ban">
                            <h2>Special Promo</h2>
                            <h1>WEEKENDS SALE</h1>
                            <span>UP TO 50% OFF</span>
                            
                        </div>
                            <div class="c-6 img-ban">
                                <<img src="./assets/imgs/banner-4.png" alt="Anh"/>
                            </div>
                        </div>
                    </section>
                    
                    <!--footer-->
                    <section class ="send">
                     <div id="toast"></div>
            <div class="grid wide">
                <div class="row">
                    <div class="col l-3 m-6 c-12">
                        <div class="footer__about">
                            <div class="footer__logo">
                                <a href="./index.html">
                                    <img src="./assets/icons/logo.svg" alt="">
                                </a>
                            </div>
                            <p>The customer is at the heart of our unique business model, which includes design.</p>
                            <a href="">
                                <img src="./assets/icons/payment.svg" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="col l-2 l-o-1 m-4 m-o-2 c-12">
                        <div class="footer__widget">
                            <h6>Shopping</h6>
                            <ul>
                                <li>
                                    <a href="">Clothing Store</a>
                                </li>

                                <li>
                                    <a href="">Trending Shoes</a>
                                </li>

                                <li>
                                    <a href="">Accessories</a>
                                </li>

                                <li>
                                    <a href="">Sale</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col l-2 m-6 c-12">
                        <div class="footer__widget">
                            <h6>Shopping</h6>
                            <ul>
                                <li>
                                    <a href="">Contact Us</a>
                                </li>
                                
                                <li>
                                    <a href="">Payment Methods</a>
                                </li>
                                
                                <li>
                                    <a href="">Delivary</a>
                                </li>
                                
                                <li>
                                    <a href="">Return &amp; Exchanges</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col l-3 l-o-1 m-4 m-o-2 c-12">
                        <div class="footer__widget">
                            <h6>NEW LETTER</h6>
                            <div class="footer__newslatter">
                                <p>Be the first to know about new arrivals, look books, sales & promos!</p>
                                <form action="" method="POST" class="form" id="newletter-form">
                                    <div class="form-group">
                                        <input id="email" name="email" rules="required|email" type="text" placeholder="email@example.com"
                                        class="form-control">
                                        <span class="form-message"></span>
                                    </div>
                                    <button type="submit">
                                        <i class="fa fa-envelope"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>         
        </section>
        <script type="module" src="./assets/js/main.js"></script>
    </body>
</html>
