import Validator from "./validator.js";
import toast from "./toast.js";
import sendRequest from "./request.js";

const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

const userItemsNavbar = $$(".user__item");
const userContents = $$(".user__content");
const avatar = $("#avatar");
const labelAvatar = $(".user__form-label--avatar");
const imageNoneWrapper = $(".user__image-none-wrapper");
const userAddressBtn = $(".user__content-addresses");
const userAddressPopup = $(".user__address-popup");
const userAddressCancelBtn = $(".user__address-btn--cancel");
const userFormPassword = new Validator(".user__form-password");
const formControls = $$(".form-control");

export default {
    handleEvents() {
        
        if (userFormPassword) {
            userFormPassword.onSubmit = (formData) => {
                const url = "MainController?action=ChangePassword";
                const data = new URLSearchParams();
                data.append("oldPassword", formData.oldPassword);
                data.append("password", formData.password);

             
                } 
            };
        },
    start() {
        this.handleEvents();
    },
};
